import type { LoaderFunction } from "remix";
import { useLoaderData, json } from "remix";
import { FogbenderSimpleWidget, Token } from "fogbender-react";
import invariant from "tiny-invariant";

type IndexData = {
  widgetToken: Token;
};

export const loader: LoaderFunction = async () => {
  invariant(process.env.FOGBENDER_ID, "FOGBENDER_ID is not defined");
  invariant(process.env.FOGBENDER_API, "FOGBENDER_API is not defined");
  invariant(process.env.FOGBENDER_SECRET, "FOGBENDER_SECRET is not defined");

  const unsignedToken = {
    widgetId: process.env.FOGBENDER_ID,
    customerId: "org123",
    customerName: "Org Name",
    userId: "example_PLEASE_CHANGE",
    userEmail: "user@example.com",
    userName: "User Name",
    userAvatarUrl:
      "https://user-images.githubusercontent.com/7026/108277328-19c97700-712e-11eb-96d6-7de0c98c9e3d.png", // optional
  };

  const res = await fetch(process.env.FOGBENDER_API, {
    method: "POST",
    headers: { Authorization: `Bearer ${process.env.FOGBENDER_SECRET}` },
    body: JSON.stringify(unsignedToken),
  });
  if (!res.ok) {
    throw new Error(`${res.status} ${res.statusText}`);
  }
  const token = (await res.json()).token;
  if (!token) {
    throw new Error("No token returned");
  }

  const data: IndexData = {
    widgetToken: token,
  };
  return json(data);
};

export default function Index() {
  const data = useLoaderData<IndexData>();
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.4" }}>
      <h1>Welcome to Remix</h1>
      <FogbenderSimpleWidget
        clientUrl="https://master--fb-client.netlify.app"
        token={data.widgetToken}
      />
    </div>
  );
}
