import Head from "next/head";
import Link from "next/link";
import { Layout } from "./Layout";

export const RoutingLayout: React.FC<React.PropsWithChildren> = ({
  children,
}) => {
  return (
    <>
      <Head>
        <title>Advanced case with Intercom-style widget</title>
      </Head>
      <div>
        <h2>Advanced case with Intercom-style widget</h2>
        <Link href="/">Back to home</Link>
        <nav>
          <Link href="/routing-trpc/">Index</Link>{" "}
          <Link href="/routing-trpc/support">Support</Link>{" "}
          <Link href="/routing-trpc/dashboard1">Dashboard1</Link>{" "}
          <Link href="/routing-trpc/dashboard2">Dashboard2</Link>
        </nav>
        <Layout>{children}</Layout>
      </div>
    </>
  );
};

export default RoutingLayout;
