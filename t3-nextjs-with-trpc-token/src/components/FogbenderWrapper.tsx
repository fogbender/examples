import {
  FogbenderFloatingWidget,
  FogbenderHeadlessWidget,
  FogbenderIsConfigured,
  FogbenderWidget,
} from "fogbender-react";
import { useRouter } from "next/router";

export function FogbenderWrapper() {
  const router = useRouter();
  const path = router.pathname;
  const show = path !== "/routing-trpc";
  const isFullscreen = path === "/routing-trpc/support";

  if (!show) {
    return null;
  }

  return (
    <FogbenderIsConfigured>
      {isFullscreen ? (
        <>
          <FogbenderWidget />
        </>
      ) : (
        <>
          <FogbenderHeadlessWidget />
          <FogbenderFloatingWidget />
        </>
      )}
    </FogbenderIsConfigured>
  );
}
