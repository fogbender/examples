import { FogbenderConfig, FogbenderProvider } from "fogbender-react";
import { trpc } from "../utils/trpc";
import { FogbenderWrapper } from "./FogbenderWrapper";

// re-using the same Layout will make sure that you won't lose the state of floating widget during navigation
export const Layout: React.FC<React.PropsWithChildren> = ({ children }) => {
  const { data } = trpc.useQuery(["user.token"]);
  return (
    <FogbenderProvider>
      {data && (
        <FogbenderConfig token={data.token} clientUrl={data.clientUrl} />
      )}
      <main>{children}</main>
      <FogbenderWrapper />
    </FogbenderProvider>
  );
};
