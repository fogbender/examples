import React from "react";
import { FogbenderSimpleWidget } from "fogbender-react";
import { InferGetServerSidePropsType } from "next";
import Link from "next/link";

export const getServerSideProps = async () => {
  // remove clientUrl bolow for real token
  const token = {
    widgetId: "dzAwMTQ5OTEzNjgyNjkwNzA3NDU2",
    customerId: "org123",
    customerName: "Org Name",
    userId: "example_PLEASE_CHANGE",
    userEmail: "user@example.com",
    // here you need to calculate proper signature on server
    userHMAC:
      "04b7c1aab187a84bfa3160b99c100df08c78b3a1e25884fc13d8d72a9b96ddc3",
    userName: "User Name",
    userAvatarUrl:
      "https://user-images.githubusercontent.com/7026/108277328-19c97700-712e-11eb-96d6-7de0c98c9e3d.png", // optional
  };
  return { props: { token } };
};

const IndexPage = (
  props: InferGetServerSidePropsType<typeof getServerSideProps>
) => {
  return (
    <div>
      <h2>Full page widget</h2>
      <Link href="/">Back to home</Link>
      <FogbenderSimpleWidget
        clientUrl="https://master--fb-client.netlify.app"
        token={props.token}
      />
    </div>
  );
};

export default IndexPage;
