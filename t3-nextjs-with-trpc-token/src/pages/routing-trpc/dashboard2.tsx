import { NextPageWithLayout } from "../_app";
import { RoutingLayout } from "../../components/RoutingLayout";
import { ReactElement } from "react";

const Page: NextPageWithLayout = () => {
  return (
    <>
      <h3>
        Dashboard: also shows Intercom-style widget, take not that state is not
        lost when navigating between dashboard pages
      </h3>
    </>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return <RoutingLayout>{page}</RoutingLayout>;
};

export default Page;
