import { NextPageWithLayout } from "../_app";
import { RoutingLayout } from "../../components/RoutingLayout";
import { ReactElement } from "react";

const Page: NextPageWithLayout = () => {
  return (
    <>
      <h3>Index: shows nothing</h3>
    </>
  );
};

Page.getLayout = function getLayout(page: ReactElement) {
  return <RoutingLayout>{page}</RoutingLayout>;
};

export default Page;
