import { FogbenderSimpleWidget, Token } from "fogbender-react";
import { useLoaderData } from "remix";

export const loader = async () => {
  const { sign, verify } = require("jsonwebtoken");

  const token = {
    widgetId: "dzAwMjc5MDIzNjY1OTk0MDEwNjI0",
    customerId: "f0001",
    customerName: "Fogbender",
    userId: "u007",
    userEmail: "jlarky@gmail.com",
    userName: "Yaroslav Lapin", // Don’t know the name? Reuse email here
    userAvatarUrl:
      "https://avatars.dicebear.com/api/avataaars/jim.lee@netflix.com.svg", // optional
  };

  const secret = "0EXC74iVL/gzBFIOQ1x+2qBrOT9WA/tH";

  // 🙋 NOTE: you can optionally also sign customerName, userEmail, and userName here for a stronger check
  const userJWT = sign(token, secret, {
    algorithm: "HS256",
  });

  return { token: { ...token, userJWT } };
};

export default function Index() {
  const { token } = useLoaderData<{ token: Token }>();
  return (
    <div style={{ fontFamily: "system-ui, sans-serif", lineHeight: "1.4" }}>
      <h1>Welcome to Remix</h1>
      <FogbenderSimpleWidget
        clientUrl="https://master--fb-client.netlify.app"
        token={token}
      />
    </div>
  );
}
